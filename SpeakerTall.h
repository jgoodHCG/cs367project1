#ifndef SpeakerTall_H
#define SpeakerTall_H
#include <vector>
#include <glm/vec3.hpp>
#include "BufferObject.h"

class SpeakerTall : public BufferObject {
private:
    float speakerTall_height;
    float speakerTall_width;
    void init_model(int level);

public:
    float height() const {
        return speakerTall_height;
    }
    void build(void *);
    void render(bool) const;
};

#endif