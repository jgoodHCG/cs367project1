#ifndef Subwoofer_H
#define Subwoofer_H
#include <vector>
#include <glm/vec3.hpp>
#include "BufferObject.h"

class Subwoofer : public BufferObject {
private:
    float subwoofer_height;
    float subwoofer_width;
    std::vector <GLushort> top_ring;
    int numptsarc;
    void init_model(int level);

public:
    float height() const {
        return subwoofer_height;
    }
    void build(void *);
    void render(bool) const;
};

#endif