#include <iostream>
#include <cmath>
#include <algorithm>

#define GLEW_STATIC
#include <GL/glew.h>
#ifdef __APPLE__
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/io.hpp>
//#include "Sphere.h"
//#include "HexNut.h"
#include "SpeakerTall.h"
#include "SpeakerShort.h"
#include "Subwoofer.h"
#include "Book.h"
#include <cstdlib>


#undef GLFW_DLL
#include <GLFW/glfw3.h>
using namespace std;
void win_refresh(GLFWwindow*);
SpeakerTall spTL;
SpeakerShort spSH;
Subwoofer sub;
Book book1, book2, book3, book4, book5;

void make_model() ;
bool outline = false;
float arc_ball_rad_square;
int screen_ctr_x, screen_ctr_y;

int bookcolor[11];
int pagecolor[11];
int bookRotate[10];

glm::mat4 camera_cf; //{glm::translate(glm::mat4(1.0f), glm::vec3{0,0,-5})};
glm::mat4 camera_cf2 = glm::translate(glm::vec3{5, -3, -15});
glm::mat4 camera_cf3 = glm::translate(glm::vec3{-5, -3, -15});
glm::mat4 camera_cf4 = glm::translate(glm::vec3{-7, -8, -15});
glm::mat4 camera_home = glm::translate(glm::vec3{0, -4, -15});;

glm::mat4 book1_cf;

void err_function (int what, const char *msg) {
    cerr << what << " " << msg << endl;
}

void win_refresh (GLFWwindow *win) {
//    cout << __PRETTY_FUNCTION__ << endl;
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);

    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity();
    /* place the camera using the camera coordinate frame */
    glMultMatrixf (glm::value_ptr(camera_cf));

    /* draw walls */
    glColor3ub (163, 85, 29);
    glBegin(GL_QUADS);
    glVertex3i(-5,0,5);
    glVertex3i(5,0,5);
    glVertex2i(5,0);
    glVertex2i(-5,0);
    glEnd();

    glColor3ub (149, 149, 80);
    glBegin(GL_QUADS);
    glVertex2i(-5,0);
    glVertex2i(5,0);
    glVertex2i(5,5);
    glVertex2i(-5,5);
    glEnd();

    glPushMatrix();
    glTranslatef(3.5, 0.0, 0.0);
    spTL.render(outline);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-4.5, 0.0, 0.0);
    spTL.render(outline);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(2, 0.0, 0.0);
    spSH.render(outline);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-3.5, 0.0, 0.0);
    spSH.render(outline);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-.75,0.0,0.0);
    sub.render(outline);
    glPopMatrix();

    //render first book
    glPushMatrix();
    glTranslatef(1.0,1.5,0);
    glMultMatrixf(glm::value_ptr(book1_cf));
    glRotated(90, 1, 0, 0);
    glRotated(90, 0, 0, 1);
    glRotated(36, 1, 0, 0);
    book1.render(outline,bookcolor[0],pagecolor[0]);
    glPopMatrix();


    // stack of books on sub
    int i;
    for (i=0;i<10;i++)
    {
        glPushMatrix();
        glScalef(.75, .50, .65);
        glTranslatef(-0.5, 3.0+i*(.35), 0);
        glMultMatrixf(glm::value_ptr(book1_cf));
        glRotatef(bookRotate[i], 0, 1, 0);
        book1.render(outline,bookcolor[i%5],pagecolor[i%3]);
        glPopMatrix();
    }


    /* must swap buffer at the end of render function */
    glfwSwapBuffers(win);
}
void key_handler (GLFWwindow *win, int key, int scan_code, int action, int mods)
{
    cout << __FUNCTION__ << endl;
    if (action != GLFW_PRESS) return;
    if (mods == GLFW_MOD_SHIFT) {
    }
    else {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(win, true);
                break;
            case GLFW_KEY_0:
                camera_cf = camera_home;
                break;
            case GLFW_KEY_1:
                camera_cf = camera_cf2;
                break;
            case GLFW_KEY_2:
                camera_cf = camera_cf3;
                break;
            case GLFW_KEY_3:
                camera_cf = camera_cf4;
                break;
            case GLFW_KEY_4:
            case GLFW_KEY_5:
            case GLFW_KEY_6:
                /* rebuild the model at different level of detail */
                if(outline == false)
                    outline = true;
                else
                    outline = false;
                break;
        }
    }
    win_refresh(win);
}
void win_resize (GLFWwindow * win, int width, int height)
{
#ifdef DEBUG
    cout << __FUNCTION__ << " " << width << "x" << height << endl;
#endif
    int w, h;
    glfwGetWindowSize(win, &w, &h);
    screen_ctr_x = w / 2.0;
    screen_ctr_y = h / 2.0;
    float rad = min(h,w)/2;
    arc_ball_rad_square = rad * rad;
    /* Use the entire window for our view port */
    glViewport(0, 0, width, height);
    /* Use GL_PROJECTION to select the type of synthetic camera */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    /* near-plane(1) & far-plane(10) are always POSITIVE and they measure
     * the distances along the Z-axis in front of the camera */
    gluPerspective(60.0, static_cast<float> (width)/ static_cast<float> (height), 1, 50); // view volume
}
void cursor_handler (GLFWwindow *win, double xpos, double ypos) {
    int state = glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_LEFT);
    static glm::vec3 first_click;
    static glm::mat4 current_cam;
    static bool is_tracking = false;

    glm::vec3 this_vec;
    if (state == GLFW_PRESS) {
        /* TODO: use glUnproject? */
        float x = (xpos - screen_ctr_x);
        float y = -(ypos - screen_ctr_y);
        float hypot_square = x * x + y * y;
        float z;

        /* determine whether the mouse is on the sphere or on the hyperbolic sheet */
        if (2 * hypot_square < arc_ball_rad_square)
            z = sqrt(arc_ball_rad_square - hypot_square);
        else
            z = arc_ball_rad_square / 2.0 / sqrt(hypot_square);
        if (!is_tracking) {
            /* store the mouse position when the button was pressed for the first time */
            first_click = glm::normalize(glm::vec3{x, y, z});
            current_cam = camera_cf;
            is_tracking = true;
        }
        else {
            /* compute the rotation w.r.t the initial click */
            this_vec = glm::normalize(glm::vec3{x, y, z});
            /* determine axis of rotation */
            glm::vec3 N = glm::cross(first_click, this_vec);

            /* determine the angle of rotation */
            float theta = glm::angle(first_click, this_vec);

            /* create a quaternion of the rotation */
            glm::quat q{cos(theta / 2), sin(theta / 2) * N};
            /* apply the rotation w.r.t to the current camera CF */
            camera_cf = current_cam * glm::toMat4(glm::normalize(q));
        }
        win_refresh(win);
    }
    else {
        is_tracking = false;
    }
}

void scroll_handler (GLFWwindow *win, double xscroll, double yscroll) {
    /* translate along the camera Z-axis */
    glm::mat4 z_translate = glm::translate((float) yscroll * glm::vec3{0, 0, 1});
    camera_cf = z_translate * camera_cf;
    win_refresh(win);
}

    void init_gl() {
        glEnable (GL_DEPTH_TEST);
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_COLOR_ARRAY);
        glEnable(GL_CULL_FACE);
        glLineWidth(3.0);

        /* place the camera at Z=+5 (notice that the sign is OPPOSITE!) */
        camera_cf *= glm::translate(glm::vec3{0, 0, -10});
    }

    void make_model() {

        int N = 0;
        spTL.build((void *)&N);
        spSH.build((void *)&N); // TODO why is N the pointer???
        sub.build((void *) &N);
        book1.build((void *) &N);
        book2.build((void *) &N);
    }

int main() {
    srand(time(NULL));
    cout << "Testing, testing..." << endl;

    for (int i = 0; i < sizeof(bookcolor) / sizeof(int); i++){
        bookcolor[i] = rand() % 5;
        if(i > sizeof(pagecolor) / sizeof(int))
        {
            pagecolor[i] = rand() % 3;
        }
    }

    for (int i = 0; i<10;i++){
        bookRotate[i] = rand()%13;
    }

    if(!glfwInit()) {
        cerr << "Can't initialize GLFW" << endl;
        glfwTerminate();
        exit (EXIT_FAILURE);
    }

    glfwSetErrorCallback(err_function);
    GLFWwindow * win;
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

    //width,height,title,*monitor,*share
    win = glfwCreateWindow(1024, 720, "Project1", NULL, NULL);

    if (!win) {
        cerr << "Can't create window" << endl;
        exit (EXIT_FAILURE);
    }

   glfwSetWindowRefreshCallback(win, win_refresh);
    /* On Mac with Retina display there is a discrepancy between units measured in
     * "screen coordinates" and "pixels" */
    glfwSetWindowSizeCallback(win, win_resize);  /* use this for non-retina displays */
    //glfwSetFramebufferSizeCallback(win, win_resize); /* use this for retina displays */
    glfwSetKeyCallback(win, key_handler);
    glfwSetCursorPosCallback(win, cursor_handler);
    glfwSetScrollCallback(win, scroll_handler);
    glfwMakeContextCurrent(win);

    /* glewInit must be invoked AFTER glfwMakeContextCurrent() */
    GLenum err = glewInit();
    if (err != GLEW_OK) {
        fprintf (stderr, "GLEW init error %s\n", glewGetErrorString(err));
        exit (EXIT_FAILURE);
    }

    /* GL functions can be called, only AFTER the window is created */
    const GLubyte *version = glGetString (GL_VERSION);
    printf ("GL Version is %s\n", version);


    glfwSetWindowSize(win, 700, 500);
    glfwSwapInterval(1);
    init_gl();
    make_model();

   /* TODO win_refresh(win); */
    while (!glfwWindowShouldClose(win)) {
        glfwWaitEvents();
    }
    glfwDestroyWindow(win);
    glfwTerminate();

    return 0;
}
