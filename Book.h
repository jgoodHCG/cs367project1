#ifndef Book_H
#define Book_H
#include <vector>
#include <glm/vec3.hpp>
#include "BufferObject.h"

class Book : public BufferObject {
private:
    int npt;
    std::vector <GLushort> top_ring, bottom_ring;
public:
    void build(void *);
    void render(bool, int, int) const;
};

#endif