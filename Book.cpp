#define _USE_MATH_DEFINES
#include <cmath>
#include <cstdlib>
#include "Book.h"

using glm::vec3;
void Book::build(void* data) {

    glGenBuffers(1, &vertex_buffer);
    glGenBuffers(1, &index_buffer);

    /*start bottom cover code */
    vec3 p0,p1,p2,p3;
    p0 = vec3{0,0,0};
    p1 = vec3{0,0,1.618};
    p2 = vec3{1,0,1.618};
    p3 = vec3(1,0,0);

    all_index.push_back(all_points.size());
    all_points.push_back(p0);
    all_index.push_back(all_points.size());
    all_points.push_back(p1);
    all_index.push_back(all_points.size());
    all_points.push_back(p2);
    all_index.push_back(all_points.size());
    all_points.push_back(p3);

    vec3 p4,p5,p6,p7;
    p4 = vec3{0,.05,0};
    p5 = vec3{0,.05,1.618};
    p6 = vec3{1,.05,1.618};
    p7 = vec3(1,.05,0);

    all_index.push_back(all_points.size());
    all_points.push_back(p4);
    all_index.push_back(all_points.size());
    all_points.push_back(p5);
    all_index.push_back(all_points.size());
    all_points.push_back(p6);
    all_index.push_back(all_points.size());
    all_points.push_back(p7);

    //front
    all_index.push_back(5);
    all_index.push_back(1);
    all_index.push_back(2);
    all_index.push_back(6);

    //right
    all_index.push_back(6);
    all_index.push_back(2);
    all_index.push_back(3);
    all_index.push_back(7);

    //back
    all_index.push_back(7);
    all_index.push_back(3);
    all_index.push_back(0);
    all_index.push_back(4);

    //left
    all_index.push_back(0);
    all_index.push_back(1);
    all_index.push_back(5);
    all_index.push_back(4);

    /*start pages code */
    vec3 p8,p9,p10,p11;
    p8 = vec3{.1,.05,0};
    p9 = vec3{.1,.05,1.608};
    p10 = vec3{1,.05,1.608};
    p11 = vec3{1,.05,0};

    all_index.push_back(all_points.size());
    all_points.push_back(p8);
    all_index.push_back(all_points.size());
    all_points.push_back(p9);
    all_index.push_back(all_points.size());
    all_points.push_back(p10);
    all_index.push_back(all_points.size());
    all_points.push_back(p11);

    vec3 p12,p13,p14,p15;
    p12 = vec3{.1,.3,0};
    p13 = vec3{.1,.3,1.608};
    p14 = vec3{1,.3,1.608};
    p15 = vec3(1,.3,0);

    all_index.push_back(all_points.size());
    all_points.push_back(p12);
    all_index.push_back(all_points.size());
    all_points.push_back(p13);
    all_index.push_back(all_points.size());
    all_points.push_back(p14);
    all_index.push_back(all_points.size());
    all_points.push_back(p15);

    //front
    all_index.push_back(13);
    all_index.push_back(9);
    all_index.push_back(10);
    all_index.push_back(14);

    //right
    all_index.push_back(14);
    all_index.push_back(10);
    all_index.push_back(11);
    all_index.push_back(15);

    //back
    all_index.push_back(15);
    all_index.push_back(11);
    all_index.push_back(8);
    all_index.push_back(12);

    //left
    all_index.push_back(8);
    all_index.push_back(9);
    all_index.push_back(13);
    all_index.push_back(12);

    /*start top cover code */
    vec3 p16,p17,p18,p19;
    p16 = vec3{0,.3,0};
    p17 = vec3{0,.3,1.618};
    p18 = vec3{1,.3,1.618};
    p19 = vec3(1,.3,0);

    all_index.push_back(all_points.size());
    all_points.push_back(p16);
    all_index.push_back(all_points.size());
    all_points.push_back(p17);
    all_index.push_back(all_points.size());
    all_points.push_back(p18);
    all_index.push_back(all_points.size());
    all_points.push_back(p19);

    vec3 p20,p21,p22,p23;
    p20 = vec3{0,.35,0};
    p21 = vec3{0,.35,1.618};
    p22 = vec3{1,.35,1.618};
    p23 = vec3(1,.35,0);

    all_index.push_back(all_points.size());
    all_points.push_back(p20);
    all_index.push_back(all_points.size());
    all_points.push_back(p21);
    all_index.push_back(all_points.size());
    all_points.push_back(p22);
    all_index.push_back(all_points.size());
    all_points.push_back(p23);

    //front
    all_index.push_back(21);
    all_index.push_back(17);
    all_index.push_back(18);
    all_index.push_back(22);

    //right
    all_index.push_back(22);
    all_index.push_back(18);
    all_index.push_back(19);
    all_index.push_back(23);

    //back
    all_index.push_back(23);
    all_index.push_back(19);
    all_index.push_back(16);
    all_index.push_back(20);

    //left
    all_index.push_back(16);
    all_index.push_back(17);
    all_index.push_back(21);
    all_index.push_back(20);

    /* spine */
    int num_pts = 1000;
    float radius = .175;
    vec3 center1 = vec3{1,.175,0};
    vec3 center2 = vec3{1,.175,1.618};

    float delta = 2 * M_PI / num_pts;

    float angle = M_PI/2;
    for(int k = 0; k < num_pts; k++){
        float x = radius * cos(angle);
        float y = radius * sin(angle);
        cout << x;
        cout << endl;
        cout << y;
        cout << endl;
        if(x>=0) {
            all_index.push_back(all_points.size());
            top_ring.push_back(all_points.size());
            all_points.push_back(vec3{x+1, y+.175, 0});
            npt += 2;
            all_index.push_back(all_points.size());
            bottom_ring.push_back(all_points.size());
            all_points.push_back(vec3{x+1, y+.175, 1.618});
        }
        angle += delta;
    }

    /* face of spine */
    all_index.push_back(all_points.size());
    all_points.push_back(center1);

    for (auto v : top_ring){
        all_index.push_back(v);

    }

    all_index.push_back(all_points.size());
    all_points.push_back(center2);

    for (auto v : bottom_ring){
        all_index.push_back(v);

    }

    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, all_points.size() * sizeof(float) *
            3, NULL, GL_DYNAMIC_DRAW);
    float *vertex_ptr = (float *) glMapBuffer(GL_ARRAY_BUFFER,
            GL_WRITE_ONLY);

    /* Initialize the vertices */
    float *ptr = vertex_ptr;
    for (auto v : all_points) {
        ptr[0] = v.x;
        ptr[1] = v.y;
        ptr[2] = v.z;
        ptr += 3;
    }
    glUnmapBuffer(GL_ARRAY_BUFFER);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    /* Initialize the indices */
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, all_index.size() * sizeof
            (GLushort), all_index.data(), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

void Book::render(bool outline, int bookrand, int pagerand) const {
    std::vector <std::vector<unsigned char>> ot = {{117,31,27},{63,20,78},{38,40,100},{48,120,33},{28,103,58}};
    std::vector <std::vector<unsigned char>> in = {{172,61,55},{95,40,114},{50,52,120},{63,114,46},{40,124,74}};
    std::vector <std::vector<unsigned char>> pg = {{249, 247, 105}, {230,228,109}, {199,196,67}};

    if(outline == false) {
        /* bind vertex buffer */
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
        glVertexPointer(3, GL_FLOAT, 0, 0);
        glDisableClientState(GL_COLOR_ARRAY);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);

        // BOTTOM COVER
        /* render the bottom face*/
        glPolygonMode(GL_FRONT, GL_FILL);
        glFrontFace(GL_CW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) 0);
        /* render the top face*/
        glFrontFace(GL_CCW);
        glColor3ub(in[bookrand][0], in[bookrand][1], in[bookrand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 4));
        /* render the front face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 8));
        /* render the right face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 12));
        /* render the back face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 16));
        /* render the left face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 20));

        // PAGES
        /* render the bottom face*/
        glPolygonMode(GL_FRONT, GL_FILL);
        glFrontFace(GL_CW);
        glColor3ub(pg[pagerand][0], pg[pagerand][1], pg[pagerand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 24));
        /* render the top face*/
        glFrontFace(GL_CCW);
        glColor3ub(pg[pagerand][0], pg[pagerand][1], pg[pagerand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 28));
        /* render the front face*/
        glFrontFace(GL_CCW);
        glColor3ub(pg[pagerand][0], pg[pagerand][1], pg[pagerand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 32));
        /* render the right face*/
        glFrontFace(GL_CCW);
        glColor3ub(pg[pagerand][0], pg[pagerand][1], pg[pagerand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 36));
        /* render the back face*/
        glFrontFace(GL_CCW);
        glColor3ub(pg[pagerand][0], pg[pagerand][1], pg[pagerand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 40));
        /* render the left face*/
        glFrontFace(GL_CCW);
        glColor3ub(pg[pagerand][0], pg[pagerand][1], pg[pagerand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 44));

        // TOP COVER
        /* render the bottom face*/
        glPolygonMode(GL_FRONT, GL_FILL);
        glFrontFace(GL_CW);
        glColor3ub(in[bookrand][0], in[bookrand][1], in[bookrand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 48));
        /* render the top face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 52));
        /* render the front face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 56));
        /* render the right face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 60));
        /* render the back face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 64));
        /* render the left face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 68));

        /* spine */
        glFrontFace(GL_CW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_QUAD_STRIP, 0, 0, npt, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 72));
        glFrontFace(GL_CCW);

        /* spine faces */
        glFrontFace(GL_CW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_TRIANGLE_FAN, 0, 0, top_ring.size() + 1, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * (72 + npt)));
        glFrontFace(GL_CCW);


        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_TRIANGLE_FAN, 0, 0, bottom_ring.size() + 1, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * (72 + npt + top_ring.size() + 1)));
        glFrontFace(GL_CCW);
    }
    else{
        /* bind vertex buffer */
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
        glVertexPointer(3, GL_FLOAT, 0, 0);
        glDisableClientState(GL_COLOR_ARRAY);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);

        // BOTTOM COVER
        /* render the bottom face*/
        glPolygonMode(GL_FRONT, GL_FILL);
        glFrontFace(GL_CW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) 0);
        /* render the top face*/
        glFrontFace(GL_CCW);
        glColor3ub(in[bookrand][0], in[bookrand][1], in[bookrand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 4));
        /* render the front face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 8));
        /* render the right face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 12));
        /* render the back face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 16));
        /* render the left face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 20));

        // PAGES
        /* render the bottom face*/
        glPolygonMode(GL_FRONT, GL_FILL);
        glFrontFace(GL_CW);
        glColor3ub(pg[pagerand][0], pg[pagerand][1], pg[pagerand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 24));
        /* render the top face*/
        glFrontFace(GL_CCW);
        glColor3ub(pg[pagerand][0], pg[pagerand][1], pg[pagerand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 28));
        /* render the front face*/
        glFrontFace(GL_CCW);
        glColor3ub(pg[pagerand][0], pg[pagerand][1], pg[pagerand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 32));
        /* render the right face*/
        glFrontFace(GL_CCW);
        glColor3ub(pg[pagerand][0], pg[pagerand][1], pg[pagerand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 36));
        /* render the back face*/
        glFrontFace(GL_CCW);
        glColor3ub(pg[pagerand][0], pg[pagerand][1], pg[pagerand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 40));
        /* render the left face*/
        glFrontFace(GL_CCW);
        glColor3ub(pg[pagerand][0], pg[pagerand][1], pg[pagerand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 44));

        // TOP COVER
        /* render the bottom face*/
        glPolygonMode(GL_FRONT, GL_FILL);
        glFrontFace(GL_CW);
        glColor3ub(in[bookrand][0], in[bookrand][1], in[bookrand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 48));
        /* render the top face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 52));
        /* render the front face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 56));
        /* render the right face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 60));
        /* render the back face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 64));
        /* render the left face*/
        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 68));

        /* spine */
        glFrontFace(GL_CW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, npt, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 72));
        glFrontFace(GL_CCW);

        /* spine faces */
        glFrontFace(GL_CW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, top_ring.size() + 1, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * (72 + npt)));
        glFrontFace(GL_CCW);


        glFrontFace(GL_CCW);
        glColor3ub(ot[bookrand][0], ot[bookrand][1], ot[bookrand][2]);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, bottom_ring.size() + 1, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * (72 + npt + top_ring.size() + 1)));
        glFrontFace(GL_CCW);
    }
}

