#define _USE_MATH_DEFINES
#include <cmath>
#include "Subwoofer.h"

using glm::vec3;
void Subwoofer::build(void* data) {

    glGenBuffers(1, &vertex_buffer);
    glGenBuffers(1, &index_buffer);

    /* define 4 corners of bottom face */
    vec3 p0,p1,p2,p3;
    p0 = vec3{0,0,0};
    p1 = vec3{0,0,1.7};
    p2 = vec3{1.5,0,1.7};
    p3 = vec3{1.5,0,0};

    all_index.push_back(all_points.size());
    all_points.push_back(p0);
    all_index.push_back(all_points.size());
    all_points.push_back(p1);
    all_index.push_back(all_points.size());
    all_points.push_back(p2);
    all_index.push_back(all_points.size());
    all_points.push_back(p3);

    /* define 4 corners of top face */
    vec3 p4,p5,p6,p7;
    p4 = vec3{0,1.5,0};
    p5 = vec3{0,1.5,1.7};
    p6 = vec3{1.5,1.5,1.7};
    p7 = vec3{1.5,1.5,0};

    all_index.push_back(all_points.size());
    all_points.push_back(p4);
    all_index.push_back(all_points.size());
    all_points.push_back(p5);
    all_index.push_back(all_points.size());
    all_points.push_back(p6);
    all_index.push_back(all_points.size());
    all_points.push_back(p7);

    /* front face order */
    all_index.push_back(5);
    all_index.push_back(1);
    all_index.push_back(2);
    all_index.push_back(6);
    /* back face order */
    all_index.push_back(7);
    all_index.push_back(3);
    all_index.push_back(0);
    all_index.push_back(4);
    /* left face order */
    all_index.push_back(4);
    all_index.push_back(0);
    all_index.push_back(1);
    all_index.push_back(5);
    /* right face order */
    all_index.push_back(6);
    all_index.push_back(2);
    all_index.push_back(3);
    all_index.push_back(7);

    /* Cylinder code starts */
    GLuint num_pts = 100;
    //int radius = 1.8;   //int! i wasn't thinking...
    float radius = 1.89;

    float delta = 2 * M_PI / num_pts;
    numptsarc = 0;

    all_index.push_back(6);
    all_index.push_back(2);
    numptsarc += 2;

    float angle = 0.0f;
    for(int k = 0; k < num_pts; k++){
        float x = radius * cos(angle);
        float z = radius * sin(angle);
        x+=.75; //.75 centers circle on box
        if(z >= 1.7 && x >= 0 && x <= 1.5 ) { //comment this out to show whole circle
            all_index.push_back(all_points.size()); //error was here, previously all_index.push_back(num_pts);
            top_ring.push_back(all_points.size());
            all_points.push_back(vec3{x, 1.5, z});
            numptsarc+=2;
            all_index.push_back(all_points.size());
            all_points.push_back(vec3{x, 0, z});
        }
        angle += delta;
    }

    all_index.push_back(5);
    all_index.push_back(1);
    numptsarc += 2;

    all_index.push_back(6);
    /* top of cylinder */
    all_index.push_back(all_points.size());
    all_points.push_back(vec3{0.75,1.5,1.7}); // mid point
    for (auto v : top_ring){
        all_index.push_back(v);
    }

    all_index.push_back(5);

    cout << numptsarc << endl;
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, all_points.size() * sizeof(float) *
            3, NULL, GL_DYNAMIC_DRAW);
    float *vertex_ptr = (float *) glMapBuffer(GL_ARRAY_BUFFER,
            GL_WRITE_ONLY);

    /* Initialize the vertices */
    float *ptr = vertex_ptr;
    for (auto v : all_points) {
        ptr[0] = v.x;
        ptr[1] = v.y;
        ptr[2] = v.z;
        ptr += 3;
    }
    glUnmapBuffer(GL_ARRAY_BUFFER);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    /* Initialize the indices */
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, all_index.size() * sizeof
            (GLushort), all_index.data(), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

void Subwoofer::render(bool outline) const {

    if (outline == false)
    {
        /* bind vertex buffer */
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
        glVertexPointer(3, GL_FLOAT, 0, 0);
        glDisableClientState(GL_COLOR_ARRAY);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);

        glPolygonMode(GL_FRONT, GL_FILL);
        glFrontFace(GL_CW);
        glColor3ub(0, 0, 0);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) 0);
        /* render the top face*/
        glFrontFace(GL_CCW);
        glColor3ub(0, 0, 0);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 4));
        /* render the front face*/
        glFrontFace(GL_CCW);
        glColor3ub(0, 0, 0);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 8));
        /* render the back  face*/
        glFrontFace(GL_CCW);
        glColor3ub(0, 0, 0);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 12));
        /* render the left face*/
        glFrontFace(GL_CCW);
        glColor3ub(0, 0, 0);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 16));
        /* render the right face*/
        glFrontFace(GL_CCW);
        glColor3ub(0, 0, 0);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 20));

        /*render mesh grill front*/
        glFrontFace(GL_CW);
        glColor3ub(10, 10, 10);
        //line below is what we had, it goes over array and draws point p0
        //glDrawRangeElements(GL_LINE_STRIP, 0, 0, 25, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 24));
        glDrawRangeElements(GL_QUAD_STRIP, 0, 0, numptsarc, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 24));
        /* render top of mesh grill */
        glFrontFace(GL_CW);
        glColor3ub(5, 5, 5);
        glDrawRangeElements(GL_TRIANGLE_FAN, 0, 0, (numptsarc / 2) + 1, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * (24 + numptsarc))); // fix numpstarc

        glFrontFace(GL_CCW); // setting it back because have to???
    }
    else
    {
        /* bind vertex buffer */
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
        glVertexPointer(3, GL_FLOAT, 0, 0);
        glDisableClientState(GL_COLOR_ARRAY);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);

        glPolygonMode(GL_FRONT, GL_FILL);
        glFrontFace(GL_CW);
        glColor3ub(0, 0, 0);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) 0);
        /* render the top face*/
        glFrontFace(GL_CCW);
        glColor3ub(0, 0, 0);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 4));
        /* render the front face*/
        glFrontFace(GL_CCW);
        glColor3ub(0, 0, 0);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 8));
        /* render the back  face*/
        glFrontFace(GL_CCW);
        glColor3ub(0, 0, 0);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 12));
        /* render the left face*/
        glFrontFace(GL_CCW);
        glColor3ub(0, 0, 0);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 16));
        /* render the right face*/
        glFrontFace(GL_CCW);
        glColor3ub(0, 0, 0);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 20));

        /*render mesh grill front*/
        glFrontFace(GL_CW);
        glColor3ub(10, 10, 10);
        //line below is what we had, it goes over array and draws point p0
        //glDrawRangeElements(GL_LINE_STRIP, 0, 0, 25, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 24));
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, numptsarc, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 24));
        /* render top of mesh grill */
        glFrontFace(GL_CW);
        glColor3ub(5, 5, 5);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, (numptsarc / 2) + 1, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * (24 + numptsarc))); // fix numpstarc

        glFrontFace(GL_CCW); // setting it back because have to???
    }
}
