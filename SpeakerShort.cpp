#define _USE_MATH_DEFINES
#include <cmath>
#include "SpeakerShort.h"

using glm::vec3;
void SpeakerShort::build(void* data) {

    glGenBuffers(1, &vertex_buffer);
    glGenBuffers(1, &index_buffer);

    /* define 4 corners of bottom face */
    vec3 p1,p2,p3,p4;
    p1 = vec3{0,0,0};
    p2 = vec3{0,0,1.2};
    p3 = vec3{1.5,0,1.2};
    p4 = vec3{1.5,0,0};

    all_index.push_back(all_points.size());
    all_points.push_back(p1);
    all_index.push_back(all_points.size());
    all_points.push_back(p2);
    all_index.push_back(all_points.size());
    all_points.push_back(p3);
    all_index.push_back(all_points.size());
    all_points.push_back(p4);

    /* define 4 corners of top face */
    vec3 p5,p6,p7,p8;
    p5 = vec3{0,2,0};
    p6 = vec3{0,2,1.2};
    p7 = vec3{1.5,2,1.2};
    p8 = vec3{1.5,2,0};

    all_index.push_back(all_points.size());
    all_points.push_back(p5);
    all_index.push_back(all_points.size());
    all_points.push_back(p6);
    all_index.push_back(all_points.size());
    all_points.push_back(p7);
    all_index.push_back(all_points.size());
    all_points.push_back(p8);



    /* right face order */
    all_index.push_back(5);
    all_index.push_back(1);
    all_index.push_back(2);
    all_index.push_back(6);
    /* left face order */
    all_index.push_back(7);
    all_index.push_back(3);
    all_index.push_back(0);
    all_index.push_back(4);
    /* front face order */
    all_index.push_back(4);
    all_index.push_back(0);
    all_index.push_back(1);
    all_index.push_back(5);
    /* back face order */
    all_index.push_back(6);
    all_index.push_back(2);
    all_index.push_back(3);
    all_index.push_back(7);




    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, all_points.size() * sizeof(float) *
            3, NULL, GL_DYNAMIC_DRAW);
    float *vertex_ptr = (float *) glMapBuffer(GL_ARRAY_BUFFER,
            GL_WRITE_ONLY);

    /* Initialize the vertices */
    float *ptr = vertex_ptr;
    for (auto v : all_points) {
        ptr[0] = v.x;
        ptr[1] = v.y;
        ptr[2] = v.z;
        ptr += 3;
    }
    glUnmapBuffer(GL_ARRAY_BUFFER);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    /* Initialize the indices */
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, all_index.size() * sizeof
    (GLushort), all_index.data(), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

}

void SpeakerShort::render(bool outline) const {
    if(outline == false) {
        /* bind vertex buffer */
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
        glVertexPointer(3, GL_FLOAT, 0, 0);
        glDisableClientState(GL_COLOR_ARRAY);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
        /* render the bottom face*/
        glPolygonMode(GL_FRONT, GL_FILL);
        glFrontFace(GL_CW);
        glColor3ub(255, 0, 0);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) 0);
        /* render the top face*/
        glFrontFace(GL_CCW);
        glColor3ub(51, 20, 0);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 4));
        /* render the front (for sure) face*/
        glFrontFace(GL_CCW);
        glColor3ub(0, 0, 0);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 8));
        /* render the back (for sure) face*/
        glFrontFace(GL_CCW);
        glColor3ub(51, 26, 0);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 12));
        /* render the left (for sure) face*/
        glFrontFace(GL_CCW);
        glColor3ub(51, 21, 0);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 16));
        /* render the right (for sure) face*/
        glFrontFace(GL_CCW);
        glColor3ub(51, 21, 0);
        glDrawRangeElements(GL_QUADS, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 20));
    }
    else{
        /* bind vertex buffer */
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
        glVertexPointer(3, GL_FLOAT, 0, 0);
        glDisableClientState(GL_COLOR_ARRAY);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
        /* render the bottom face*/
        glPolygonMode(GL_FRONT, GL_FILL);
        glFrontFace(GL_CW);
        glColor3ub(255, 0, 0);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) 0);
        /* render the top face*/
        glFrontFace(GL_CCW);
        glColor3ub(51, 20, 0);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 4));
        /* render the front (for sure) face*/
        glFrontFace(GL_CCW);
        glColor3ub(0, 0, 0);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 8));
        /* render the back (for sure) face*/
        glFrontFace(GL_CCW);
        glColor3ub(51, 26, 0);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 12));
        /* render the left (for sure) face*/
        glFrontFace(GL_CCW);
        glColor3ub(51, 21, 0);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 16));
        /* render the right (for sure) face*/
        glFrontFace(GL_CCW);
        glColor3ub(51, 21, 0);
        glDrawRangeElements(GL_LINE_STRIP, 0, 0, 4, GL_UNSIGNED_SHORT, (void *) (sizeof(GLushort) * 20));
    }

}