#ifndef SpeakerShort_H
#define SpeakerShort_H
#include <vector>
#include <glm/vec3.hpp>
#include "BufferObject.h"

class SpeakerShort : public BufferObject {
private:
    float speakerShort_height;
    float speakerShort_width;
    void init_model(int level);

public:
    float height() const {
        return speakerShort_height;
    }
    void build(void *);
    void render(bool) const;
};

#endif